clear;
clc;

limit = 1000;
nn = 1:20:limit;
A = [];
B = [];

for n = 1:20:limit
    r1  = 1 + rand(n,1); % ~U[1,2]
%     r1  = rand(n,1); % ~U[0,1]
%     r1  = -1 + 2*rand(n,1); % ~U[-1,1]
    
    a1 = mean(1./r1);
    A = [A a1];
    
    b1 = 1/mean(r1);
    B = [B b1];
end
plot(nn,A, nn,B)
legend('E[1/X]','1/E[X]')


