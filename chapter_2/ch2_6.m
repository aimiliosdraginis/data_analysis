clear;
clc;

res = zeros(1,1000);

for i=1:10000
    n = rand(1,100);
    Y = mean(n);
    res(i) = Y;
end

h = histogram(res);



%% using summation
% for i=1:10000
%     n = rand(1,100);
%     Y = sum(n);
%     res(i) = Y;
% end
% 
% h = histogram(res);