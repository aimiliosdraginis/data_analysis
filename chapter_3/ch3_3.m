clc;
clear;

lamda = 1/15;
n = 2;
dof = n-1;

cntr = 0;
for i = 1:1000
    r_arr = exprnd(lamda, n, 1);
    mn = mean(r_arr);
    vr = var(r_arr);

    ret = tinv(0.975, dof);

    conf_right = mn + ret*(sqrt(vr/n));
    conf_left = mn - ret*(sqrt(vr/n));
    
    if lamda > conf_left && lamda < conf_right
        cntr = cntr + 1;
    end
end

disp("inside confidence interval percentage: " + cntr/1000);