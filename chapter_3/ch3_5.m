clc;
clear;

A = readmatrix('eruption.dat', 'Filetype', 'text', 'Delimiter', '\t');


dof = length(A) - 1;

var_conf = zeros(3,2);
mean_conf = zeros(3,2);
for i = 1:3
    
    vr = var(A(:,i));
    left = chi2inv(0.025, dof);
    right = chi2inv(0.975, dof);

    var_conf(i,1) = sqrt((dof*vr)/right);
    var_conf(i,2) =  sqrt((dof*vr)/left);
    
    
    
    mn = mean(A(:,i));
    ret = tinv(0.975, dof);

    mean_conf(i,2) = mn + ret*(sqrt(vr/length(A(:,i))));
    mean_conf(i,1) = mn - ret*(sqrt(vr/length(A(:,i))));
    
    [h2, p] = chi2gof(A(:,i));
    if h2 == 1
        disp('rejected')
    else
        disp('accepted')
    end

end