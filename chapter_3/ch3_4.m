clc;
clear;


arr = [41 46 47 47 48 50 50 50 50 50 50 50 48 50 50 50 50 50 50 50 52 52 53 55 50 50 50 50 52 52 53 53 53 53 53 57 52 52 53 53 53 53 53 53 54 54 55 68];

dof = length(arr) - 1;

vr = var(arr);
left = chi2inv(0.025, dof);
right = chi2inv(0.975, dof);

conf_right = (dof*vr)/right;
conf_left =  (dof*vr)/left;

h1 = vartest(arr, 5^2);
if h1 == 1
    disp('rejected')
else
    disp('accepted')
end

mn = mean(arr);
ret = tinv(0.975, dof);

conf_right = mn + ret*(sqrt(vr/length(arr)));
conf_left = mn - ret*(sqrt(vr/length(arr)));


[h2, p] = chi2gof(arr);
if h2 == 1
    disp('rejected')
else
    disp('accepted')
end